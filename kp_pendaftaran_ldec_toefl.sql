-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 17, 2020 at 10:04 AM
-- Server version: 10.3.21-MariaDB-1:10.3.21+maria~bionic-log
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kp_pendaftaran_ldec_toefl`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_informasi`
--

CREATE TABLE `tb_informasi` (
  `id_informasi` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `informasi` text NOT NULL,
  `waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_informasi`
--

INSERT INTO `tb_informasi` (`id_informasi`, `judul`, `informasi`, `waktu`) VALUES
(1, 'Test', 'test aja', '2020-01-17 09:39:07');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jadwal`
--

CREATE TABLE `tb_jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `tipe_kelas` set('LDEC','TOEFL') NOT NULL,
  `status_jadwal` set('dibuka','ditutup') NOT NULL DEFAULT 'dibuka'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_jadwal`
--

INSERT INTO `tb_jadwal` (`id_jadwal`, `tanggal`, `tipe_kelas`, `status_jadwal`) VALUES
(1, '2019-12-25', 'LDEC', 'dibuka'),
(2, '2019-12-14', 'LDEC', 'dibuka'),
(3, '2019-12-14', 'TOEFL', 'dibuka'),
(4, '2019-12-14', 'TOEFL', 'dibuka');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kelas`
--

CREATE TABLE `tb_kelas` (
  `id_kelas` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `nama_kelas` varchar(50) NOT NULL,
  `nama_pengajar` varchar(50) NOT NULL,
  `ruangan_kelas` varchar(10) NOT NULL,
  `jam_kelas` varchar(10) NOT NULL,
  `batas_peserta` int(11) NOT NULL DEFAULT 20,
  `status_kelas` set('aktif','penuh','nonaktif','selesai') NOT NULL DEFAULT 'aktif'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_kelas`
--

INSERT INTO `tb_kelas` (`id_kelas`, `id_jadwal`, `nama_kelas`, `nama_pengajar`, `ruangan_kelas`, `jam_kelas`, `batas_peserta`, `status_kelas`) VALUES
(2, 1, 'A', 'A', 'A', 'A', 1, 'aktif'),
(3, 3, 'T1', 'T1', 'T1', 'T1', 20, 'aktif'),
(4, 4, 'T2', 'T2', 'T2', 'T2', 20, 'selesai');

-- --------------------------------------------------------

--
-- Table structure for table `tb_notifikasi`
--

CREATE TABLE `tb_notifikasi` (
  `id_notifikasi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `message` varchar(50) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_notifikasi`
--

INSERT INTO `tb_notifikasi` (`id_notifikasi`, `id_user`, `message`, `date`) VALUES
(1, 4, 'Pendaftaran anda telah diverifikasi.', '2020-01-16 11:48:06'),
(2, 7, 'Pendaftaran anda telah diverifikasi.', '2020-01-17 01:52:12'),
(3, 7, 'Pendaftaran anda telah diverifikasi.', '2020-01-17 01:54:22'),
(4, 7, 'Pendaftaran anda telah diverifikasi.', '2020-01-17 01:54:55');

-- --------------------------------------------------------

--
-- Table structure for table `tb_peserta`
--

CREATE TABLE `tb_peserta` (
  `id_peserta` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `bukti_pembayaran` varchar(50) DEFAULT NULL,
  `status_pembayaran` set('menunggu','terverifikasi','ditolak') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_peserta`
--

INSERT INTO `tb_peserta` (`id_peserta`, `id_jadwal`, `id_kelas`, `id_user`, `bukti_pembayaran`, `status_pembayaran`) VALUES
(4, 1, 0, 7, '9df65a86ab1d2e3180bc33f65f48717c.pdf', 'terverifikasi');

-- --------------------------------------------------------

--
-- Table structure for table `tb_users`
--

CREATE TABLE `tb_users` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `tipe_user` set('admin','mahasiswa') NOT NULL,
  `status_user` set('aktif','nonaktif') NOT NULL DEFAULT 'aktif',
  `email` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `nim` varchar(8) DEFAULT NULL,
  `jenis_kelamin` varchar(50) NOT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_users`
--

INSERT INTO `tb_users` (`id_user`, `username`, `password`, `tipe_user`, `status_user`, `email`, `nama`, `nim`, `jenis_kelamin`, `telepon`, `alamat`) VALUES
(2, 'admin', 'admin', 'admin', 'aktif', NULL, NULL, NULL, '', NULL, NULL),
(3, 'test', 'test', 'mahasiswa', 'aktif', 'test@email.com', 'Test', '16013099', 'Laki-laki', '08123456789', 'Test'),
(4, 'aku', 'aku', 'mahasiswa', 'aktif', 'aku@aku', 'Aku', '12345', 'Laki-laki', '123', 'Aku'),
(5, 'admin', 'admin', 'mahasiswa', 'aktif', 'admin@localhost', 'admin', '12341', 'Laki-laki', '080812312', '123123'),
(6, '123', 'qwd', 'mahasiswa', 'aktif', '123@123', 'dasd', '16013039', 'Laki-laki', 'asdsa', 'dsd'),
(7, 'Ref', 'ref', 'mahasiswa', 'aktif', '16013039@unikadelasalle.ac.id', 'Ref', '16013039', 'Laki-laki', '123123', '123123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_informasi`
--
ALTER TABLE `tb_informasi`
  ADD PRIMARY KEY (`id_informasi`);

--
-- Indexes for table `tb_jadwal`
--
ALTER TABLE `tb_jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indexes for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  ADD PRIMARY KEY (`id_kelas`),
  ADD KEY `id_jadwal` (`id_jadwal`);

--
-- Indexes for table `tb_notifikasi`
--
ALTER TABLE `tb_notifikasi`
  ADD PRIMARY KEY (`id_notifikasi`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_peserta`
--
ALTER TABLE `tb_peserta`
  ADD PRIMARY KEY (`id_peserta`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `status_permbayaran` (`status_pembayaran`),
  ADD KEY `id_jadwal` (`id_jadwal`);

--
-- Indexes for table `tb_users`
--
ALTER TABLE `tb_users`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `nim` (`nim`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_informasi`
--
ALTER TABLE `tb_informasi`
  MODIFY `id_informasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_jadwal`
--
ALTER TABLE `tb_jadwal`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_notifikasi`
--
ALTER TABLE `tb_notifikasi`
  MODIFY `id_notifikasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_peserta`
--
ALTER TABLE `tb_peserta`
  MODIFY `id_peserta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_users`
--
ALTER TABLE `tb_users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_peserta`
--
ALTER TABLE `tb_peserta`
  ADD CONSTRAINT `tb_peserta_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `tb_users` (`id_user`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
