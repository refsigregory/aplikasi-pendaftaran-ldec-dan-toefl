<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

        public function __construct()
        {
            parent::__construct();

            // auto load model
            $this->load->model('auth_model');
            $this->load->model('informasi_model');
        }

        public function index()
        {
            if($this->session->userdata('logged_in')){
                redirect('home', 'refresh');
            }

                    $head['title'] = $this->config->item("site_name");
                    $this->load->view('include/header', $head);
    
                    $data['name'] = $this->config->item("site_name");
                    $data['informasi'] = $this->informasi_model->getAll();
                    $this->load->view('informasi', $data);
    
                    if(isset($_GET['join']))
                    {
                        $this->informasi_model->delete($_GET['delete']);
                        redirect('home/informasi');
                    }
                    $foot['name'] = $data['name'];
                    $foot['showCopyright'] = false;
                    $this->load->view('include/footer', $foot);
        }
        public function login()
        {  
                $this->auth_model->logged_in();
                $head['title'] = "Login Page";
                $head['loginPage'] = true;
                $this->load->view('include/header', $head);

                $this->load->view('include/login');
                $foot['showCopyright'] = false;
                $this->load->view('include/footer', $foot);
        }

        public function checkLogin()
        {
            // cek login
            $this->auth_model->logged_in();
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            // memanggil fungsi cek login
            $this->auth_model->login($username, $password);
        }

        public function register()
        {
            // Halaman Register
                
            $this->auth_model->logged_in();
            $head['title'] = "Pendaftaran";
            $head['loginPage'] = true;
            $this->load->view('include/header', $head);

            $this->load->view('include/register');
            $foot['showCopyright'] = false;
            $this->load->view('include/footer', $foot);
        }

            public function newRegister()
    {
		$data = $this->input->post();
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
            }
            
            if(explode("@", $data['email'])[1] != 'unikadelasalle.ac.id')
            {
                $err[] = "Hanya diijinkan menggunakan email Unika De La Salle Manado";
            }

            if(strlen($data['nim']) != 8)
            {
                $err[] = "NIM tidak sesuai format";
            }

			if(!isset($err))
			{
                $data = array_merge($data, ["tipe_user" => 'mahasiswa','status_user'=>'aktif']);
                $query = $this->user_model->insert($data);
                //if($query) sendMail($data['email'], "Pendaftaran Berhasil", "Akun anda telah aktif, anda bisa memilih kelas dari jadwal yang sedang tersedia.");
				$msg = "Berhasil Mendaftar";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('auth/login', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('auth/register', 'refresh');
			}
		}else {
			$msg = "Terjadi kesalahan";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('auth/register', 'refresh');
		}
    }

        public function logout()
        {
            // FUnsi Logout
            $this->session->sess_destroy();
            redirect('auth/login');
        }

        public function sendMail($email, $subject, $text) {
            $this->load->library('email');
            
            $kode = base64_encode(time());
            $message = '<p>'.$text.'</p>';
            
            // Get full html:
            $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
                <title>' . html_escape($subject) . '</title>
                <style type="text/css">
                    body {
                        font-family: Arial, Verdana, Helvetica, sans-serif;
                        font-size: 16px;
                    }
                </style>
            </head>
            <body>
            ' . $message . '
            </body>
            </html>';
            // Also, for getting full html you may use the following internal method:
            //$body = $this->email->full_html($subject, $message);
            
            if(isset($email)) {
                $result = $this->email
                        ->from('refsisangkay@gmail.com') // sesuaikan email pengirim
                        //->reply_to('yoursecondemail@somedomain.com')    // Optional, an account where a human being reads.
                        ->to($email)
                        ->subject($subject)
                        ->message($body)
                        ->send();
                return $result;
            }
            //var_dump($result);
            //echo '<br />';
            echo $this->email->print_debugger();
            
            //exit;
                }
}
