<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    public function __construct ()
    {
        parent::__construct();
        $this->auth_model->check();
        $this->load->model('jadwal_model');
        $this->load->model('kelas_model');
		$this->load->model('peserta_model');
		$this->load->model('informasi_model');
    }
	public function index()
	{
            
                $head['title'] = $this->config->item("site_name");
                $this->load->view('include/header', $head);

                $this->load->view('include/sidemenu');

                $data['name'] = $this->config->item("site_name");
                $data['notif'] = $this->user_model->getNotifByUser($this->session->userdata('id'));
                $this->load->view('home', $data);

                $foot['name'] = $data['name'];
                $this->load->view('include/footer', $foot);
    }
    
    public function mahasiswa()
	{
                $head['title'] = $this->config->item("site_name");
                $this->load->view('include/header', $head);

                $this->load->view('include/sidemenu');

                $data['name'] = $this->config->item("site_name");
                $data['mahasiswa'] = $this->user_model->getMahasiswa();
                $this->load->view('mahasiswa', $data);

                if(isset($_GET['tambah']))
                {
                    $this->load->view('modal_tambah_user.php');
                }

                if(isset($_GET['edit']))
                {
                    $data['edit'] = $this->user_model->getByID($_GET['edit'])[0];
                    $this->load->view('modal_ubah_user.php', $data);
                }

                if(isset($_GET['delete']))
                {
                    $this->user_model->delete($_GET['delete']);
                    redirect('home/mahasiswa');
                }


                if(isset($_GET['join']))
                {
                    $this->user_model->delete($_GET['delete']);
                    redirect('home/mahasiswa');
                }
                $foot['name'] = $data['name'];
                $this->load->view('include/footer', $foot);
    }
    
    public function tambahUser()
    {
		$data = $this->input->post();
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
                $data = array_merge($data, ["tipe_user" => 'mahasiswa']);
                $this->user_model->insert($data);
				$msg = "Data User Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/mahasiswa', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/mahasiswa', 'refresh');
			}
		}else {
			$msg = "Data Mahasiswa tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/mahasiswa', 'refresh');
		}
    }
    public function editUser()
    {
		$data = $this->input->post();
		if(isset($data['id_user'])) {
            $id_user = $data['id_user'];
            unset($data['id_user']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
                //$data = array_merge($data, ["tipe_user" => 'mahasiswa']);
                $this->user_model->update($id_user, $data);
				$msg = "Data User Berhasil Diubah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/mahasiswa', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/mahasiswa', 'refresh');
			}
		}else {
			$msg = "Data Mahasiswa tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/mahasiswa', 'refresh');
		}
	}
	

    public function informasi()
	{
                $head['title'] = $this->config->item("site_name");
                $this->load->view('include/header', $head);

                $this->load->view('include/sidemenu');

                $data['name'] = $this->config->item("site_name");
                $data['informasi'] = $this->informasi_model->getAll();
                $this->load->view('informasi', $data);

                if(isset($_GET['tambah']))
                {
                    $this->load->view('modal_tambah_informasi.php');
                }

                if(isset($_GET['edit']))
                {
                    $data['edit'] = $this->informasi_model->getByID($_GET['edit'])[0];
                    $this->load->view('modal_ubah_informasi.php', $data);
                }

                if(isset($_GET['delete']))
                {
                    $this->informasi_model->delete($_GET['delete']);
                    redirect('home/informasi');
                }


                if(isset($_GET['join']))
                {
                    $this->informasi_model->delete($_GET['delete']);
                    redirect('home/informasi');
                }
                $foot['name'] = $data['name'];
                $this->load->view('include/footer', $foot);
	}
	

    public function tambahInformasi()
    {
		$data = $this->input->post();
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
                $data = array_merge($data, ["waktu" => date("Y-m-d H:i:s")]);
                $this->informasi_model->insert($data);
				$msg = "Informasi  Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/informasi', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/informasi', 'refresh');
			}
		}else {
			$msg = "Informasi tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/informasi', 'refresh');
		}
    }
    public function editInformasi()
    {
		$data = $this->input->post();
		if(isset($data['id_informasi'])) {
            $id_informasi = $data['id_informasi'];
            unset($data['id_informasi']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
                //$data = array_merge($data, ["tipe_user" => 'mahasiswa']);
                $this->informasi_model->update($id_informasi, $data);
				$msg = "Informasi Berhasil Diubah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/informasi', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/informasi', 'refresh');
			}
		}else {
			$msg = "Informasi tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/informasi', 'refresh');
		}
	}
	


    public function kelas()
	{
                $head['title'] = $this->config->item("site_name");
                $this->load->view('include/header', $head);

                $this->load->view('include/sidemenu');

				$data['name'] = $this->config->item("site_name");
				if(isset($_GET['filter'])){
					if($_GET['filter'] == 'Semua'):
						$data['kelas'] = $this->kelas_model->getAll();
					else:
						$data['kelas'] = $this->kelas_model->getAllByType($_GET['filter']);
					endif;
				} else {
					$data['kelas'] = $this->kelas_model->getAll();
				}
                
                $data['jadwal'] = $this->jadwal_model->getAll();
				$this->load->view('kelas', $data);
				
				

                if(isset($_GET['tambah']))
                {
                    $this->load->view('modal_tambah_kelas.php');
                }

                if(isset($_GET['edit']))
                {
                    $data['edit'] = $this->kelas_model->getByID($_GET['edit'])[0];
                    $this->load->view('modal_ubah_kelas.php', $data);
                }

                if(isset($_GET['delete']))
                {
                    $this->kelas_model->delete($_GET['delete']);
                    redirect('home/kelas');
                }

                if(isset($_GET['join']))
                {
                    $this->load->view('modal_gabung_kelas.php');
                }

                if(isset($_GET['lihat']))
                {
                    $data['daftar_peserta'] = $this->peserta_model->getValidByKelas($_GET['lihat']);
                    $this->load->view('modal_peserta_kelas.php',$data);
                }

                if(isset($_GET['hapus_peserta']))
                {
                    $this->peserta_model->delete($_GET['hapus_peserta'],$_GET['kelas']);
                    redirect('home/kelas');
                }

                $foot['name'] = $data['name'];
                $this->load->view('include/footer', $foot);
    }

    public function tambahKelas()
    {
		$data = $this->input->post();
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
                //$data = array_merge($data, ["tipe_user" => 'mahasiswa']);
                $this->kelas_model->insert($data);
				$msg = "Data User Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/kelas', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/kelas', 'refresh');
			}
		}else {
			$msg = "Data Mahasiswa tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/kelas', 'refresh');
		}
    }

    public function editKelas()
    {
        $id_kelas = $_POST['id_kelas'];
        $data = $this->input->post();
        unset($data['id_kelas']);
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
                //$data = array_merge($data, ["t	ipe_user" => 'mahasiswa']);
                $this->kelas_model->update($id_kelas, $data);
				$msg = "Data Kelas Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/kelas', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/kelas', 'refresh');
			}
		}else {
			$msg = "Data Kelas tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/kelas', 'refresh');
		}
    }

    public function joinKelas()
    {
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);

		$bukti_pembayaran = "";

		//if($_POST[''] == "sudah"):
			if ( ! $this->upload->do_upload('bukti_pembayaran'))
			{
					$err[] = $this->upload->display_errors();
			}
			else
			{
					$data = array('upload_data' => $this->upload->data());
					$bukti_pembayaran = $data['upload_data']['file_name'];
			}
        //endif;
        
		$data = $this->input->post();
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
            }
            
            

			if(!isset($err))
			{
				$data = array_merge($data, ["id_user" => $this->session->userdata('id'), "bukti_pembayaran" => $bukti_pembayaran, "status_pembayaran" => 'menunggu']);
				
				if($this->peserta_model->countPesertaByKelas($data['id_kelas']) <= $this->kelas_model->getByID($data['id_kelas'])[0]->batas_peserta || $data['id_kelas'] == '0'):
					$this->peserta_model->insert($data);
					$msg = "Data Berhasil Mendaftar";
					$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
					redirect('home/kelas', 'refresh');
				else:
					$msg = "Kelas sudah penuh";
					$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
					redirect('home/kelas', 'refresh');
				endif;


			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/kelas', 'refresh');
			}
		}else {
			$msg = "Data Mahasiswa tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/kelas', 'refresh');
		}
    }

    public function jadwal()
	{
                $head['title'] = $this->config->item("site_name");
                $this->load->view('include/header', $head);

                $this->load->view('include/sidemenu');

                $data['name'] = $this->config->item("site_name");
				
				if(isset($_GET['filter'])){
					if($_GET['filter'] == 'Semua'):
						$data['jadwal'] = $this->user_model->getJadwal();
					else:
						$data['jadwal'] = $this->jadwal_model->getAllByType($_GET['filter']);
					endif;
				} else {
					$data['jadwal'] = $this->user_model->getJadwal();
				}
                $this->load->view('jadwal', $data);

                if(isset($_GET['tambah']))
                {
                    $this->load->view('modal_tambah_jadwal.php');
                }

                if(isset($_GET['edit']))
                {
                    $data['edit'] = $this->jadwal_model->getByID($_GET['edit'])[0];
                    $this->load->view('modal_ubah_jadwal.php', $data);
                }

                if(isset($_GET['delete']))
                {
                    $this->jadwal_model->delete($_GET['delete']);
                    redirect('home/jadwal');
                }

                $foot['name'] = $data['name'];
                $this->load->view('include/footer', $foot);
    }

    public function tambahJadwal()
    {
		$data = $this->input->post();
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
                //$data = array_merge($data);
                $this->jadwal_model->insert($data);
				$msg = "Data User Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/jadwal', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/jadwal', 'refresh');
			}
		}else {
			$msg = "Data Mahasiswa tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/jadwal', 'refresh');
		}
    }

    public function editJadwal()
    {
        $id_jadwal = $_POST['id_jadwal'];
        $data = $this->input->post();
        unset($data['id_jadwal']);
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
                //$data = array_merge($data);
                $this->jadwal_model->update($id_jadwal,$data);
				$msg = "Data User Berhasil Diubah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/jadwal', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/jadwal', 'refresh');
			}
		}else {
			$msg = "Data Mahasiswa tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/jadwal', 'refresh');
		}
    }

    public function peserta()
	{
                $head['title'] = $this->config->item("site_name");
                $this->load->view('include/header', $head);

                $this->load->view('include/sidemenu');

				$data['name'] = $this->config->item("site_name");
				
				
				
				if(isset($_GET['filter'])){
					if($_GET['filter'] == 'Semua'):
						$data['peserta'] = $this->peserta_model->getAll();
					else:
						$data['peserta'] = $this->peserta_model->getAllByType($_GET['filter']);
					endif;
				} else {
					$data['peserta'] = $this->peserta_model->getAll();
				}


                $data['mahasiswa'] = $this->user_model->getMahasiswa();
                $data['kelas'] = $this->kelas_model->getAll();
                $this->load->view('peserta', $data);

                if(isset($_GET['terima']))
                {
					$user = $this->user_model->getByID($_GET['id_user']);
                    $query = $this->peserta_model->update($_GET['terima'], ["status_pembayaran" => "terverifikasi"]);
                    $msg = "Perserta berhasil diverifikasi";
                    $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
					$this->user_model->insertNotif($_GET['id_user'], "Pendaftaran anda telah diverifikasi.");
					if($query) $this->sendMail($user[0]->email, "Pendaftaran Berhasil", "Pendaftaran and atelah diverifikasi. Harap masuk ke akun anda untuk melihat informasi terbaru.");
                    redirect('home/peserta');
                }

                if(isset($_GET['tolak']))
                {
					$user = $this->user_model->getByID($_GET['id_user']);
                    $this->peserta_model->update($_GET['tolak'], ["status_pembayaran" => "ditolak"]);
                    $msg = "Peserta Ditolak";
                    $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
					$this->user_model->insertNotif($_GET['id_user'], "Pendaftaran anda ditolak.");
					if($query) $this->sendMail($user[0]->email, "Pendaftaran Ditolak", "Pendaftaran anda untuk kelas dengan Jadwal sebelumnya ditolak. Periksa kembali buti pembayaran yang di upload.");
                    redirect('home/peserta');
                }

                if(isset($_GET['tambah']))
                {
                    $this->load->view('modal_tambah_peserta.php');
                }

                $foot['name'] = $data['name'];
                $this->load->view('include/footer', $foot);
    }

    public function tambahPeserta()
    {
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);

		$bukti_pembayaran = "";

		if(false):
			if ( ! $this->upload->do_upload('bukti_pembayaran'))
			{
					$err[] = $this->upload->display_errors();
			}
			else
			{
					$data = array('upload_data' => $this->upload->data());
					$bukti_pembayaran = $data['upload_data']['file_name'];
			}
        endif;
        
		$data = $this->input->post();
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
            }
            
            

			if(!isset($err))
			{
                $data = array_merge($data, ["bukti_pembayaran" => $bukti_pembayaran, "status_pembayaran" => 'terverifikasi']);
                $this->peserta_model->insert($data);
				$msg = "Data Berhasil Menambah Peserta";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/peserta', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/peserta', 'refresh');
			}
		}else {
			$msg = "Data Peserta tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/kelas', 'refresh');
		}
	}
	
	public function sendMail($email, $subject, $text) {
        $this->load->library('email');
        
        $kode = base64_encode(time());
        $message = '<p>'.$text.'</p>';
        
        // Get full html:
        $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
            <title>' . html_escape($subject) . '</title>
            <style type="text/css">
                body {
                    font-family: Arial, Verdana, Helvetica, sans-serif;
                    font-size: 16px;
                }
            </style>
        </head>
        <body>
        ' . $message . '
        </body>
        </html>';
        // Also, for getting full html you may use the following internal method:
        //$body = $this->email->full_html($subject, $message);
        
        if(isset($email)) {
            $result = $this->email
                    ->from('refsisangkay@gmail.com') // sesuaikan email pengirim
                    //->reply_to('yoursecondemail@somedomain.com')    // Optional, an account where a human being reads.
                    ->to($email)
                    ->subject($subject)
                    ->message($body)
                    ->send();
            return $result;
        }
        //var_dump($result);
        //echo '<br />';
        echo $this->email->print_debugger();
        
        //exit;
            }
}
