<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta_model extends CI_Model
{
    public function getAll()
    {
        $query=$this->db->query("select * from tb_peserta");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getAllByType($id)
    {
        $query=$this->db->query("select * from tb_peserta join tb_jadwal on tb_jadwal.id_jadwal = tb_peserta.id_jadwal where tb_jadwal.tipe_kelas = '$id'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getByID($id)
    {
        $query=$this->db->query("select * from tb_peserta where id_peserta = '$id'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getByJadwalKelasUser($jadwal = "", $kelas, $user)
    {
        $query=$this->db->query("select * from tb_peserta where id_kelas = '$kelas' and id_user = '$user'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function userAdalahPeserta($user)
    {
        $query=$this->db->query("select * from tb_peserta where id_user = '$user' and status_pembayaran != 'ditolak'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getByKelas($kelas)
    {
        $query=$this->db->query("select * from tb_peserta where id_kelas = '$kelas'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getValidByKelas($kelas)
    {
        $query=$this->db->query("select * from tb_peserta where id_kelas = '$kelas' and status_pembayaran = 'terverifikasi'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getCountByKelas($kelas)
    {
        $query=$this->db->query("select * from tb_peserta where id_kelas = '$kelas'");
        return $query->num_rows();
    }

    public function countPesertaByKelas($kelas)
    {
        $query=$this->db->query("select * from tb_peserta where id_kelas = '$kelas' and status_pembayaran = 'terverifikasi'");
        return $query->num_rows();
    }


    public function insert($data)
    {
        $query = $this->db->insert("tb_peserta", $data);
        if($query) 
        {
            return true;
        } else {
            return false;
        }
    }

    public function update($id, $data)
    {
        $this->db->where("id_peserta", $id);
        $query = $this->db->update("tb_peserta", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function delete($user, $kelas)
    {
        $query = $this->db->query("delete from tb_peserta where id_kelas = '$kelas' and id_user = '$user'");
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }
}