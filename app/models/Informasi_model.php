<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Informasi_model extends CI_Model
{
    public function getAll()
    {
        $query=$this->db->query("select * from tb_informasi order by waktu desc");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getByID($id)
    {
        $query=$this->db->query("select * from tb_informasi where id_informasi = $id");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function insert($data)
    {
        $query = $this->db->insert("tb_informasi", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function update($id, $data)
    {
        $this->db->where("id_informasi", $id);
        $query = $this->db->update("tb_informasi", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id)
    {
        $this->db->where("id_kelas", $id);
        $query = $this->db->delete("tb_kelas");
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }
}