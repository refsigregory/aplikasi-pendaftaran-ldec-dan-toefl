
<div id="content-wrapper">

<div class="container-fluid">

  <!-- Page Content -->
  <h1>Beranda</h1>
    <hr>
    <?php if($this->session->userdata('status') == 'nonaktif'):?>
      <div class="alert alert-warning">Akun anda saat ini belum aktif.</div>
    <?php endif;?>
    <?php if($notif != ""): foreach($notif as $row):?>
      <div class="alert alert-info"><?=$row->message;?></div>
    <?php endforeach; endif;?>
    <p>Selamat Datang.</p>

</div>
<!-- /.container-fluid -->
