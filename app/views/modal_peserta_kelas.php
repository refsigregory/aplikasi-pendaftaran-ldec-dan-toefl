
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Peserta <?=$this->jadwal_model->getByID($this->kelas_model->getByID($_GET['lihat'])[0]->id_jadwal)[0]->tipe_kelas;?> <?=$this->kelas_model->getByID($_GET['lihat'])[0]->nama_kelas;?> (<?=$this->jadwal_model->getByID($this->kelas_model->getByID($_GET['lihat'])[0]->id_jadwal)[0]->tanggal;?>)</h4>
      </div>
      
      <div class="modal-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>NIM</th>
                    <th>Nama Mahasiswa</th>
                  </tr>
                </thead>
                <tbody>
                    <?php if($daftar_peserta != ""): $no = 1; foreach($daftar_peserta as $row): ?>
                  <tr>
                    <td><?=$no++;?></td>
                    <td><?=$this->user_model->getByID($row->id_user)[0]->nim;?></td>
                    <td><?=$this->user_model->getByID($row->id_user)[0]->nama;?></td>
                    <?php if($this->session->userdata('role') == 'admin'):?>
                    <td><a href="?hapus_peserta=<?=$row->id_user;?>&kelas=<?=$row->id_kelas;?>" class="btn btn-danger">Hapus</a></td>
                    <?php endif;?>
                    <?php endforeach; endif;?>
                </tbody>
              </table>
            </div>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->