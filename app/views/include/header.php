<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?=$title;?></title>

  <!-- Custom fonts for this template-->
  <link href="<?=base_url();?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="<?=base_url();?>assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?=base_url();?>assets/css/sb-admin.css" rel="stylesheet">

</head>
<?php
if(!isset($loginPage)): $loginPage = false; endif;
?>
<body id="page-top">
<?php if(!$loginPage):?>
  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="<?=base_url();?>"> <img src="<?=base_url('assets/img/logo.png');?>" width="32px" height="32px"> Pendaftaran LDEC/TOEFL</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">

    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
    <?php  if($this->session->userdata('logged_in')):?>

      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-circle fa-fw"></i> <?=$this->session->userdata('username');?> (<?=$this->session->userdata('role');?>)
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Keluar</a>
        </div>
      </li>
    <?php else:?>

      <li class="nav-item ">
        <a class="nav-link" href="<?=base_url('auth/login');?>" id="userDropdown" role="button">
          <i class="fas fa-user fa-fw"></i> Masuk
        </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="<?=base_url('auth/register');?>" id="userDropdown" role="button">
          <i class="fas fa-users fa-fw"></i> Mendaftar
        </a>
      </li>
      
    <?php endif;?>
    </ul>

  </nav>
<?php endif;?>
  <div id="wrapper">

