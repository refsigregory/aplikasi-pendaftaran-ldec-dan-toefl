<?php if(!isset($showNavbar)): $showNavbar = true; endif;?>
<?php if($showNavbar):?>
    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url();?>">
          <i class="fas fa-fw fa-home"></i>
          <span>Beranda</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?=base_url('home/informasi');?>">
          <i class="fas fa-fw fa-info"></i>
          <span>Informasi</span>
        </a>
      </li>

      <?php if($this->session->userdata('role') == 'admin'):?>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url('home/mahasiswa');?>">
          <i class="fas fa-fw fa-users"></i>
          <span>Daftar Mahasiswa</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url('home/jadwal');?>">
          <i class="fas fa-fw fa-calendar"></i>
          <span>Jadwal</span></a>
      </li>
      <?php endif;?>

      <?php if($this->session->userdata('status') == 'aktif'):?>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url('home/kelas');?>">
          <i class="fas fa-fw fa-file"></i>
          <span>Kelas</span></a>
      </li>
      <?php endif;?>

      <?php if($this->session->userdata('role') == 'admin'):?>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url('home/peserta');?>">
          <i class="fas fa-fw fa-users"></i>
          <span>Peserta</span></a>
      </li>
      <?php endif;?>
    </ul>
<?php endif;?>
