<div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header text-center">
        <img src="<?=base_url('assets/img/logo.png');?>" width="30%">
        <br>
        <h1 style="font-size: 14pt;padding: 20px;">UPT PUSAT PENGEMBANGAN BAHASA</h1>
      </div>
      <div class="card-body">
                <p>
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>
        <form action="<?=base_url('auth/checkLogin')?>" method="post">
          <div class="form-group">
            <div class="form-label-group">
              <input name="username" class="form-control" required="required">
              <label for="inputEmail">Nama Pengguna</label>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input type="password" name="password" class="form-control" required="required">
              <label for="inputPassword">Kata Sandi</label>
            </div>
          </div>
          <button class="btn btn-primary btn-block" type="submit">Masuk</button>
          <a class="btn btn-success btn-block" href="<?=base_url('auth/register');?>">Buat Akun</a>
        </form>
        <div class="text-center">
        </div>
      </div>
    </div>
  </div>