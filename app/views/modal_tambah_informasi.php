
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Informasi</h4>
      </div>
      
      <div class="modal-body">
            <?=form_open_multipart(base_url('home/tambahInformasi'));?>
                
                <div class="form-group">
                    <label>Judul</label>
                    <input type="text" class="form-control" name="judul" placeholder="">
                </div>


                <div class="form-group">
                    <label>Informasi</label>
                    <textarea class="form-control" name="informasi"></textarea>
                </div>

                <button type="submit" class="btn btn-primary btn-block">Tambah</button>
            </form>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->