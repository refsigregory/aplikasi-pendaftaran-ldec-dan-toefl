
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Peserta</h4>
      </div>
      
      <div class="modal-body">
            <?=form_open_multipart(base_url('home/tambahPeserta'));?>

    
                <div class="form-group">
                    <label>Mahasiswa</label>
                    <select class="form-control" name="id_user" id="">
                    <option value=""> - Pilih Mahasiswa - </option>
                    <?php if($mahasiswa != ""): foreach($mahasiswa as $row):?>
                    <option value="<?=$row->id_user;?>"><?=$row->nama;?></option>
                    <?php endforeach; endif;?>
                    </select>
                </div>
                
                <div class="form-group">
                    <label>Kelas</label>
                    <select class="form-control" name="id_kelas" id="">
                    <option value=""> - Pilih Kelas - </option>
                    <?php if($kelas != ""): foreach($kelas as $row):?>
                    <option value="<?=$row->id_kelas;?>"><?=$this->jadwal_model->getByID($this->kelas_model->getByID($row->id_kelas)[0]->id_jadwal)[0]->tipe_kelas;?> <?=$this->kelas_model->getByID($row->id_kelas)[0]->nama_kelas;?> (<?=$this->jadwal_model->getByID($this->kelas_model->getByID($row->id_kelas)[0]->id_jadwal)[0]->tanggal;?>)</option>
                    <?php endforeach; endif;?>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary btn-block">Tambah</button>
            </form>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->