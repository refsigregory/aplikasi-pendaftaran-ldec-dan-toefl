
<div id="content-wrapper">

<div class="container-fluid">

  <!-- Page Content -->
  <h1>Jadwal</h1>
    <hr>
    <p>
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-users"></i>
            Daftar Jadwal
            <a class="btn btn-primary" href="<?=base_url('home/jadwal/?tambah');?>">TAMBAH</a>
          </div>
          <div class="card-body">
          <div class="form">
            <form>
                <div class="form-group">
                    <label>Tipe Kelas:</label>
                    <div class="row">
                        <div class="col-xs-2">
                            <select class="form-control" name="filter">
                              <option>Semua</option>
                              <option>LDEC</option>
                              <option>TOEFL</option>
                            </select>
                        </div>
                        <div class="col-xs-2">
                            <button class="btn btn-primary">Tampilkan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        
          <p>
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Jadwal</th>
                    <th>Tipe Kelas</th>
                    <th>Status Jadwal</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    <?php if($jadwal != ""): $no = 1; foreach($jadwal as $row): ?>
                  <tr>
                    <td><?=$no++;?></td>
                    <td><?=$row->tanggal;?></td>
                    <td><?=$row->tipe_kelas;?></td>
                    <td><?=$row->status_jadwal;?></td>
                    <td>
                        <a class="btn btn-warning" href="<?=base_url('home/jadwal?edit='. $row->id_jadwal);?>">Ubah</a>
                        <a class="btn btn-danger" href="<?=base_url('home/jadwal?delete='. $row->id_jadwal);?>" onclick="return Tanya()">Hapus</a>
                    </td>
                  </tr>
                    <?php endforeach; endif;?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </p>

</div>
<!-- /.container-fluid -->
