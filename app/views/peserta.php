
<div id="content-wrapper">

<div class="container-fluid">

  <!-- Page Content -->
  <h1>Peserta</h1>
  <hr>
    <p>
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-users"></i>
            Peserta Pendaftar   <a class="btn btn-primary" href="<?=base_url('home/peserta/?tambah');?>">TAMBAH</a>
  
          </div>
          <div class="card-body">
          <div class="form">
            <form>
                <div class="form-group">
                    <label>Tipe Kelas:</label>
                    <div class="row">
                        <div class="col-xs-2">
                            <select class="form-control" name="filter">
                              <option>Semua</option>
                              <option>LDEC</option>
                              <option>TOEFL</option>
                            </select>
                        </div>
                        <div class="col-xs-2">
                            <button class="btn btn-primary">Tampilkan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Jadwal</th>
                    <th>Kelas</th>
                    <th>Bukti Pembayaran</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    <?php if($peserta != ""): $no = 1; foreach($peserta as $row): ?>
                  <tr>
                    <td><?=$no++;?></td>
                    <td><?=$this->user_model->getByID($row->id_user)[0]->nama;?></td>
                    <td>LDEC <?=$this->jadwal_model->getByID($row->id_jadwal)[0]->tanggal;?></td>
                    <?php if($this->jadwal_model->getByID($row->id_jadwal)[0]->tipe_kelas == 'LDEC'):?> 
                    <td>-</td>
                    <?php else:?>
                      <td><?=$this->jadwal_model->getByID($this->kelas_model->getByID($row->id_kelas)[0]->id_jadwal)[0]->tipe_kelas;?> <?=$this->kelas_model->getByID($row->id_kelas)[0]->nama_kelas;?> (<?=$this->jadwal_model->getByID($this->kelas_model->getByID($row->id_kelas)[0]->id_jadwal)[0]->tanggal;?>)</td>
                    <?php endif;?>
                    <td><a href="<?=base_url('uploads/');?><?=$this->peserta_model->getByID($row->id_peserta)[0]->bukti_pembayaran;?>" target="_blank">Lihat Bukti</a></td>
                    <td><?=$this->peserta_model->getByID($row->id_peserta)[0]->status_pembayaran;?></td>
                    <td>
                    <?php if($this->peserta_model->getByID($row->id_peserta)[0]->status_pembayaran == 'menunggu'):?>
                        <a class="btn btn-warning" href="<?=base_url('home/peserta?terima='. $row->id_peserta.'&id_user='.$row->id_user);?>">Terima</a>
                        <a class="btn btn-danger" href="<?=base_url('home/peserta?tolak='. $row->id_peserta.'&id_user='.$row->id_user);?>" onclick="return Tanya()">Tolak</a>
                    <?php endif;?>
                    </td>
                  </tr>
                    <?php endforeach; endif;?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </p>

</div>
<!-- /.container-fluid -->
