
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Kelas</h4>
      </div>
      
      <div class="modal-body">
            <?=form_open_multipart(base_url('home/editKelas'));?>
            <input type="hidden" name="id_kelas" value="<?=$this->input->get('edit');?>">
                <div class="form-group">
                    <label>Jadwal Kelas</label>
                    <select class="form-control" name="id_jadwal" id="">
                    <option value=""> - Pilih Jadwal - </option>
                    <?php if($jadwal != ""): foreach($jadwal as $row):?>
                    <option value="<?=$row->id_jadwal;?>" <?=($edit->id_jadwal == $row->id_jadwal ? 'selected="seleced"' : '');?>><?=$row->tipe_kelas . " " . $row->tanggal;?></option>
                    <?php endforeach; endif;?>
                    </select>
                </div>
                
                <div class="form-group">
                    <label>Nama Kelas</label>
                    <input type="text" class="form-control" name="nama_kelas" value="<?=$edit->nama_kelas;?>">
                </div>

                <div class="form-group">
                    <label>Nama Pengajar</label>
                    <input type="text" class="form-control" name="nama_pengajar" value="<?=$edit->nama_kelas;?>">
                </div>

                <div class="form-group">
                    <label>Ruangan Kelas</label>
                    <input type="text" class="form-control" name="ruangan_kelas" value="<?=$edit->nama_kelas;?>">
                </div>

                <div class="form-group">
                    <label>Jam Kelas</label>
                    <input type="text" class="form-control" name="jam_kelas" value="<?=$edit->nama_kelas;?>">
                </div>

                <div class="form-group">
                    <label>Batas Peserta</label>
                    <input type="text" class="form-control" name="batas_peserta" value="<?=$edit->batas_peserta;?>">
                </div>

                <div class="form-group">
                    <label>Status Kelas</label>
                    <select class="form-control" name="status_kelas" id="">
                    <option value=""> - Pilih Status Kelas - </option>
                    <option <?=($edit->status_kelas == 'aktif' ? 'selected="seleced"' : '');?>>aktif</option>
                    <option <?=($edit->status_kelas == 'nonaktif' ? 'selected="seleced"' : '');?>>nonaktif</option>
                    <option <?=($edit->status_kelas == 'penuh' ? 'selected="seleced"' : '');?>>penuh</option>
                    <option <?=($edit->status_kelas == 'selesai' ? 'selected="seleced"' : '');?>>selesai</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary btn-block">Ubah</button>
            </form>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->