
<div id="content-wrapper">

<div class="container-fluid">

  <!-- Page Content -->
  <h1>Kelas</h1>
    <hr>
    <p>
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-users"></i>
            Daftar Kelas
            <?php if($this->session->userdata('role') == 'admin'):?>
              <a class="btn btn-primary" href="<?=base_url('home/kelas/?tambah');?>">TAMBAH</a>
            <?php endif;?>
          </div>
          <div class="card-body">
          <p>
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>

            <div class="form">
            <form>
                <div class="form-group">
                    <label>Tipe Kelas:</label>
                    <div class="row">
                        <div class="col-xs-2">
                            <select class="form-control" name="filter">
                              <option>Semua</option>
                              <option>LDEC</option>
                              <option>TOEFL</option>
                            </select>
                        </div>
                        <div class="col-xs-2">
                            <button class="btn btn-primary">Tampilkan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Jadwal</th>
                    <th>Nama Kelas</th>
                    <th>Pengajar</th>
                    <th>Ruangan (Jam)</th>
                    <th>Jumlah Peserta</th>
                    <th>Status Kelas</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    <?php if($kelas != ""): $no = 1; foreach($kelas as $row):
                    // Ubah status kelas jika sudah penuh
                    if($this->peserta_model->countPesertaByKelas($row->id_kelas) >= $this->kelas_model->getByID($row->id_kelas)[0]->batas_peserta):  
                      $this->kelas_model->update($row->id_kelas, ["status_kelas"=> 'penuh']);
                    endif;
                      ?>
                  <tr>
                    <td><?=$no++;?></td>
                    <td><?=$this->jadwal_model->getByID($row->id_jadwal)[0]->tipe_kelas;?> <?=$this->jadwal_model->getByID($row->id_jadwal)[0]->tanggal;?></td>
                    <td><?=$row->nama_kelas;?></td>
                    <td><?=$row->nama_pengajar;?></td>
                    <td><?=$row->ruangan_kelas;?> (<?=$row->jam_kelas;?>)</td>
                    <td><?=$this->peserta_model->countPesertaByKelas($row->id_kelas);?>/<?=$row->batas_peserta;?></td>
                    <td><?=$row->status_kelas;?></td>
                    <td>
                    <?php if($this->session->userdata('role') == 'admin'):?>
                        <a class="btn btn-info" href="<?=base_url('home/kelas?lihat='. $row->id_kelas);?>">Lihat Peserta</a>
                        <a class="btn btn-warning" href="<?=base_url('home/kelas?edit='. $row->id_kelas);?>">Ubah</a>
                        <a class="btn btn-danger" href="<?=base_url('home/kelas?delete='. $row->id_kelas);?>" onclick="return Tanya()">Hapus</a>
                    <?php else:?>
                    <?php if($this->peserta_model->getByJadwalKelasUser($row->id_jadwal, $row->id_kelas, $this->session->userdata('id')) == "" && $row->status_kelas == 'aktif' && $this->peserta_model->userAdalahPeserta($this->session->userdata('id')) == ""):?>

                      <?php if($this->jadwal_model->getByID($row->id_jadwal)[0]->tipe_kelas == 'LDEC'):?>    
                        <a class="btn btn-info" href="<?=base_url('home/kelas?join='. $row->id_jadwal);?>">Gabung</a>
                      <?php else:?>
                        <a class="btn btn-info" href="<?=base_url('home/kelas?join='. $row->id_jadwal.'&kelas='.$row->id_kelas);?>">Gabung</a>
                      <?php endif;?>
                    <?php else:?>
                        <a class="btn btn-info" href="<?=base_url('home/kelas?lihat='. $row->id_kelas);?>">Lihat Peserta</a>
                    <?php endif; endif;?>
                    </td>
                  </tr>
                    <?php endforeach; endif;?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </p>

</div>
<!-- /.container-fluid -->
