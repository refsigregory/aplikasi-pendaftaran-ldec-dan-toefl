
<div id="content-wrapper">

<div class="container-fluid">

  <!-- Page Content -->
  <h1>Mahasiswa</h1>
    <hr>
    <p>
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-users"></i>
            Data Mahasiswa
            <a class="btn btn-primary" href="<?=base_url('home/mahasiswa/?tambah');?>">TAMBAH</a>
          </div>
          <div class="card-body">
          <p>
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Telepon</th>
                    <th>Alamat</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    <?php if($mahasiswa != ""): $no = 1; foreach($mahasiswa as $row): ?>
                  <tr>
                    <td><?=$no++;?></td>
                    <td><?=$row->nama;?></td>
                    <td><?=$row->email;?></td>
                    <td><?=$row->telepon;?></td>
                    <td><?=$row->alamat;?></td>
                    <td><?=$row->status_user;?></td>
                    <td>
                        <a class="btn btn-warning" href="<?=base_url('home/mahasiswa?edit='. $row->id_user);?>">Ubah</a>
                        <a class="btn btn-danger" href="<?=base_url('home/mahasiswa?delete='. $row->id_user);?>" onclick="return Tanya()">Hapus</a>
                    </td>
                  </tr>
                    <?php endforeach; endif;?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </p>

</div>
<!-- /.container-fluid -->
