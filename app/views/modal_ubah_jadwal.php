
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Jadwal</h4>
      </div>
      
      <div class="modal-body">
            <?=form_open_multipart(base_url('home/editJadwal'));?>
            <input type="hidden" name="id_jadwal" value="<?=$this->input->get('edit');?>">

                <div class="form-group">
                    <label>Tanggal</label>
                    <input type="text" class="form-control" name="tanggal" value="<?=$edit->tanggal;?>" placeholder="">
                </div>

    
                <div class="form-group">
                    <label>Tipe Kelas</label>
                    <select class="form-control" name="tipe_kelas" id="">
                    <option value=""> - Pilih Tipe Kelas - </option>
                    <option <?=($edit->tipe_kelas == 'LDEC' ? 'selected="seleced"' : '');?>>LDEC</option>
                    <option <?=($edit->tipe_kelas == 'TOEFL' ? 'selected="seleced"' : '');?>>TOEFL</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Status Jadwal</label>
                    <select class="form-control" name="status_jadwal" id="">
                    <option value=""> - Pilih Status Jadwal - </option>
                    <option <?=($edit->status_jadwal == 'dibuka' ? 'selected="seleced"' : '');?>>dibuka</option>
                    <option <?=($edit->status_jadwal == 'ditutup' ? 'selected="seleced"' : '');?>>ditutup</option>
                    </select>
                </div>
                

                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
            </form>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->