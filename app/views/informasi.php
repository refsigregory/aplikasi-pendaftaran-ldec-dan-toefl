
<div id="content-wrapper">

<div class="container-fluid">

  <!-- Page Content -->
  <h1>Informasi</h1>
    <hr>
    <?php if($this->session->userdata('role') == 'admin'):?>
    <p>
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-users"></i>
            Data Informasi
            <a class="btn btn-primary" href="<?=base_url('home/informasi/?tambah');?>">TAMBAH</a>
          </div>
          <div class="card-body">
          <p>
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Informasi</th>
                    <th>Waktu</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    <?php if($informasi != ""): $no = 1; foreach($informasi as $row): ?>
                  <tr>
                    <td><?=$no++;?></td>
                    <td><?=$row->judul;?></td>
                    <td><?=$row->informasi;?></td>
                    <td><?=$row->waktu;?></td>
                    <td>
                        <a class="btn btn-warning" href="<?=base_url('home/informasi?edit='. $row->id_informasi);?>">Ubah</a>
                        <a class="btn btn-danger" href="<?=base_url('home/informasi?delete='. $row->id_informasi);?>" onclick="return Tanya()">Hapus</a>
                    </td>
                  </tr>
                    <?php endforeach; endif;?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </p>
    <?php else:?>
    <p>
    <?php if($informasi != ""): $no = 1; foreach($informasi as $row): ?>
        <div class="card mb-3">
          <div class="card-header">
            <?=$row->judul;?>
            <small class="float-right"><?=$row->waktu;?></small>
          </div>
          <div class="card-body">
            <p><?=$row->informasi;?></p>
          </div>
        </div>
    <?php endforeach; endif;?>
    </p>

    <?php endif;?>

</div>
<!-- /.container-fluid -->
