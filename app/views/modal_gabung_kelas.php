
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Peserta Baru</h4>
      </div>
      
      <div class="modal-body">
            <?=form_open_multipart(base_url('home/joinKelas'));?>
                <input type="hidden" name="id_jadwal" value="<?=$this->input->get('join');?>">
                <input type="hidden" name="id_kelas" value="<?=($this->input->get('kelas') != "") ? $this->input->get('kelas'): '0';?>">

                <div class="form-group">
                    <label>Bukti Pembayaran</label>
                    <input type="file" class="form-control" name="bukti_pembayaran">
                </div>

                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
            </form>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->